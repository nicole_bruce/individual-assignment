import express from "express";
import { v4 as uuidv4 } from "uuid";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

import User from "./models/user.model";
import Entry from "./models/entries.model";
import CV from "./models/cv.model";

const router = express.Router();
const errorArray = [];
const verifyEmail = (email) =>
  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
    email
  );
const validateRequestBody = (properties, req) => {
  errorArray.length = 0;
  properties.forEach((item) => {
    if (req.body.hasOwnProperty(item) === false) {
      errorArray.push(`${item}`);
    }
  });
};
const validateEmail = (email) => {
  if (email) {
    if (verifyEmail(email) === false) {
      errorArray.push("email");
    }
  }
};
const validationResponse = (res) => {
  if (errorArray.length !== 0) {
    res.status(400).send({
      message: "validation error",
      invalid: errorArray,
    });
    return true;
  } else {
    return false;
  }
};
const checkToken = (req, res, next) => {
  const token = req.headers.token;
  if (!token) {
    return res.status(403).send({
      message: "token not provided",
    });
  }
  jwt.verify(token, "jwtsecret", (err) => {
    if (err) {
      return res.status(403).send({
        message: err.message,
      });
    } else {
      next();
    }
  });
};

router.get("/", (req, res) => {
  res.send("Hello World!");
});

//create contact entry
router.post("/contact_form/entries", async (req, res) => {
  const properties = ["name", "email", "phoneNumber", "content"];
  try {
    validateRequestBody(properties, req);
    validateEmail(req.body.email);
    const validationError = await validationResponse(res);
    if (!validationError) {
      let newEntry = {
        id: uuidv4(),
        ...req.body,
      };
      Entry.create(newEntry, (err) => {
        if (err) {
          return res.send(err);
        }
        res.status(201).send(newEntry);
      });
    }
  } catch (err) {
    console.log(err);
  }
});

//create new user
router.post("/users", async (req, res) => {
  errorArray.length = 0;
  const properties = ["name", "email", "password"];
  try {
    validateRequestBody(properties, req);
    validateEmail(req.body.email);
    if (req.body.password) {
      if (req.body.password.length < 8) {
        errorArray.push(`password`);
      }
    }
    const validationError = await validationResponse(res);
    if (!validationError) {
      let newUser = {
        id: uuidv4(),
        name: req.body.name,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 2),
      };
      const createdUser = {
        id: newUser.id,
        name: newUser.name,
        email: newUser.email,
        password: newUser.password,
      };
      User.create(newUser, (err) => {
        if (err) {
          return res.send(err);
        }
        res.status(201).send(createdUser);
      });
    }
  } catch (err) {
    console.log(err);
  }
});

// login
router.post("/auth", async (req, res) => {
  const properties = ["email", "password"];
  try {
    validateRequestBody(properties, req);
    validateEmail(req.body.email);
    const validationError = await validationResponse(res);
    if (!validationError) {
      const users = await User.findAll();
      const checkUser = users.filter((user) => user.email === req.body.email);
      if (checkUser.length === 0) {
        return res.status(401).send({
          message: "incorrect credentials provided",
        });
      } else if (checkUser.length > 0) {
        if (bcrypt.compareSync(req.body.password, checkUser[0].password)) {
          const payload = {
            email: checkUser[0].email,
            time: new Date(),
          };
          const token = jwt.sign(payload, "jwtsecret", {
            expiresIn: "22h",
          });
          return res.status(200).send({ token: token });
        } else {
          return res.status(401).send({
            message: "incorrect credentials provided",
          });
        }
      }
    }
  } catch (err) {
    console.log(err);
  }
});

// get all contact entries
router.get("/contact_form/entries", checkToken, async (req, res) => {
  try {
    let entries = await Entry.findAll();
    res.status(200).send(entries);
  } catch (err) {
    throw new Error(err);
  }
});

// get single contact entry
router.get("/contact_form/entries/:id", checkToken, async (req, res) => {
  try {
    let entry = await Entry.findById(req.params.id);
    res.status(200).send(entry);
  } catch (err) {
    throw new Error(err);
  }
});

//create cv skill
router.post("/cv", checkToken, async (req, res) => {
  const properties = ["skill", "category"];
  try {
    validateRequestBody(properties, req);
    const validationError = await validationResponse(res);
    if (!validationError) {
      let newCV = {
        id: uuidv4(),
        created_at: new Date(),
        updated_at: new Date(),
        ...req.body,
      };
      CV.create(newCV, (err) => {
        if (err) {
          return res.send(err);
        }
        res.status(201).send(newCV);
      });
    }
  } catch (err) {
    console.log(err);
  }

  // READ/get all cv skills
  router.get("/cv", checkToken, async (req, res) => {
    try {
      let skills = await CV.findAll();
      res.status(200).send(skills);
    } catch (err) {
      throw new Error(err);
    }
  });

  // UPDATE cv
  router.put("/cv/:id", checkToken, async (req, res) => {
    try {
      const updatedCV = {
        ...req.body,
        updated_at: new Date(),
      };
      await CV.update(req.params.id, updatedCV);
      res
        .status(200)
        .send({ data: updatedCV, meta: { success: "skill updated!" } });
    } catch (err) {
      throw new Error(err);
    }
  });

  // Delete cv
  router.delete("/cv/:id", checkToken, async (req, res) => {
    try {
      await CV.delete(req.params.id);
      res.status(200).send({ success: "Cv skill deleted!" });
    } catch (err) {
      throw new Error(err);
    }
  });
});

export default router;
