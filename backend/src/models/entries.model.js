"use strict";
import dbConn from "../../config/db.config";

//user object create
class Entry {
  constructor(entry) {
    this.id = entry.id;
    this.name = entry.name;
    this.email = entry.email;
    this.phoneNumber = entry.phoneNumber;
    this.content = entry.content;
  }

  static create(newEntry, result) {
    dbConn.query("INSERT INTO entries set ?", newEntry, function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(err, null);
      } else {
        console.log(res.insertId);
        result(null, res.insertId);
      }
    });
  }

  static async findById(id) {
    const result = await new Promise((resolve, reject) => {
      dbConn.query(
        "Select * from entries where id = ? ",
        id,
        function (err, res) {
          if (err) {
            console.log("error: ", err);
            reject(err);
          } else {
            resolve(res);
          }
        }
      );
    });
    return result;
  }

  static async findAll() {
    const result = await new Promise((resolve, reject) => {
      dbConn.query("Select * from entries", function (err, res) {
        if (err) {
          console.log("error: ", err);
          reject(err);
        } else {
          console.log("entries : ", res);
          resolve(res);
        }
      });
    });
    return result;
  }

  static update(id, entry, result) {
    dbConn.query(
      "UPDATE entries SET name=?,email=?,phoneNumber=?, content=?, WHERE id = ?",
      [entry.name, entry.email, entry.phoneNumber, entry.content, id],
      function (err, res) {
        if (err) {
          console.log("error: ", err);
          result(null, err);
        } else {
          result(null, res);
        }
      }
    );
  }

  static delete(id, result) {
    dbConn.query("DELETE FROM entries WHERE id = ?", [id], function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        result(null, res);
      }
    });
  }
}

export default Entry;
