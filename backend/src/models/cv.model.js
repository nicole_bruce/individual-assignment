"use strict";
import dbConn from "../../config/db.config";

//user object create
class CV {
  constructor(cv) {
    this.id = cv.id;
    this.skill = cv.skill;
    this.category = cv.category
    this.created_at = new Date();
    this.updated_at = new Date();
  }

  static create(newCV, result) {
    dbConn.query("INSERT INTO cv set ?", newCV, function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(err, null);
      } else {
        console.log(res.insertId);
        result(null, res.insertId);
      }
    });
  }

  static async findById(id) {
    const result = await new Promise((resolve, reject) => {
      dbConn.query("Select * from cv where id = ? ", id, function (err, res) {
        if (err) {
          console.log("error: ", err);
          reject(err);
        } else {
          resolve(res);
        }
      });
    });
    return result;
  }

  static async findAll() {
    const result = await new Promise((resolve, reject) => {
      dbConn.query("Select * from cv", function (err, res) {
        if (err) {
          console.log("error: ", err);
          reject(err);
        } else {
          console.log("cv: ", res);
          resolve(res);
        }
      });
    });
    return result;
  }

  static async update(id, cv) {
    const result = await new Promise((resolve, reject) => {
      dbConn.query(
        "UPDATE cv SET skill=?, category=?, updated_at=? WHERE id = ?",
        [cv.skill, cv.category, cv.updated_at, id],
        function (err, res) {
          if (err) {
            console.log("error: ", err);
            reject(err);
          } else {
            resolve(res);
          }
        }
      );
    });
    return result;
  }

  static async delete(id) {
    const result = await new Promise((resolve, reject) => {
      dbConn.query("DELETE FROM cv WHERE id = ?", [id], function (err, res) {
        if (err) {
          console.log("error: ", err);
          reject(err);
        } else {
          resolve(res);
        }
      });
    });
    return result;
  }
}

export default CV;
