"use strict";
import dbConn from "../../config/db.config";

//user object create
class User {
  constructor(user) {
    this.id = user.id;
    this.name = user.name;
    this.email = user.email;
    this.password = user.password;
  }
  static create(newUser, result) {
    dbConn.query("INSERT INTO users set ?", newUser, function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(err, null);
      } else {
        console.log(res.insertId);
        result(null, res.insertId);
      }
    });
  }
  static findById(id, result) {
    dbConn.query("Select * from users where id = ? ", id, function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(err, null);
      } else {
        result(null, res);
      }
    });
  }
  static async findAll() {
    const result = await new Promise((resolve, reject) => {
      dbConn.query("Select * from users", function (err, res) {
        if (err) {
          console.log("error: ", err);
          reject (err);
        } else {
          console.log("users : ", res);
          resolve (res);
        }
      });
    });
    return result
  }
  static update(id, user, result) {
    dbConn.query(
      "UPDATE users SET name=?,email=?,password=? WHERE id = ?",
      [user.name, user.email, user.password, id],
      function (err, res) {
        if (err) {
          console.log("error: ", err);
          result(null, err);
        } else {
          result(null, res);
        }
      }
    );
  }
  static delete(id, result) {
    dbConn.query("DELETE FROM users WHERE id = ?", [id], function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        result(null, res);
      }
    });
  }
}

export default User;
